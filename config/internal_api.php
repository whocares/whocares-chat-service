<?php

return [

    'hcp_service_url' => env('HCP_SERVICE_URL', 'http://nginx-hcp'),
    'patient_service_url' => env('PATIENT_SERVICE_URL', 'http://nginx-patient'),

];
