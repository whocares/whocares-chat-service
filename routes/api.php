<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'auth:api'], function () {

    Route::post('broadcast', 'BroadcastController@broadcast');

    Route::group(['prefix' => 'chats'], function () {
        Route::get('/', 'ChatController@index');
        Route::get('{chat}', 'ChatController@show');
        Route::post('create', 'ChatController@store');
    });

    Route::group(['prefix' => 'messages'], function () {
        Route::post('send/{chat}', 'MessageController@store');
        Route::get('fetch-messages/{chat}', 'MessageController@fetchMessages');
    });

    Route::get('dev', function () {
        dd(request()->header());
    });

});
