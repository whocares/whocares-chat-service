SHA=$(git rev-parse HEAD)
IMAGE_NAME=${REPO_NAME}:${SHA}

docker build -q -t ${REPO_NAME}:latest -t ${IMAGE_NAME} -f Dockerfile .
docker push ${REPO_NAME}:latest
docker push ${IMAGE_NAME}

echo IMAGE NAME = "$IMAGE_NAME"
echo CONTAINER NAME = "$K8_CONTAINER_NAME"
echo DEPLOYMENT NAME = "$K8_DEPLOYMENT_NAME"

kubectl set image deployments/"$K8_DEPLOYMENT_NAME" "$K8_CONTAINER_NAME"="$IMAGE_NAME"
