<?php

namespace App\Events;

use App\Models\Message;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class MessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Message details.
     *
     * @var Message
     */
    public $message;

    /**
     * Name of the user who sent the message.
     *
     * @var string
     */
    public $name;

    /**
     * Create a new event instance.
     *
     * @param  Message  $message
     * @param  string   $name
     */
    public function __construct(Message $message, string $name)
    {
        $this->message = $message;
        $this->name = $name;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        $channels = [];

        foreach ($this->message->chat->users as $user) {
            if ($user['user_id'] != $this->message->user_id) {
                $channels[] = new PrivateChannel("users.{$user['user_id']}");
            }
        }

        return $channels;
    }
}
