<?php

namespace App\Events;

use App\Models\Chat;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class ChatCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Chat details.
     *
     * @var Chat
     */
    public $chat;

    /**
     * Create a new event instance.
     *
     * @param  Chat  $chat
     */
    public function __construct(Chat $chat)
    {
        $this->chat = $chat;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        $channels = [];

        logger($this->chat);

        foreach ($this->chat->users as $user) {
            if ($user['user_id'] != Auth::user()->getId()) {
                $channels[] = new PrivateChannel("users.{$user['user_id']}");
            }
        }

        logger($channels);

        return $channels;
    }
}
