<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreChat extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO: Check if the user is authorized to create the chat.
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'chat.users' => ['required', 'array', 'min:1'],
            'chat.users.*.user_id' => ['required', 'string'],
            'chat.users.*.scope' => ['required', 'string', Rule::in(['health-care-provider', 'patient'])]
        ];
    }
}
