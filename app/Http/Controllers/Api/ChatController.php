<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreChat;
use App\Http\Resources\Chat\ChatCollection;
use App\Http\Resources\Chat\ChatResource;
use App\Jobs\CreateChat;
use App\Models\Chat;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ChatController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  Request  $request
     * @return ChatCollection
     */
    public function index(Request $request): ChatCollection
    {
        $chats = Chat::query()
            ->where('users.user_id', Auth::user()->getId())
            ->paginate();

        ChatCollection::wrap('chats');
        return new ChatCollection($chats);
    }

    /**
     * Display the specified resource.
     *
     * @param  Chat  $chat
     * @return ChatResource
     */
    public function show(Chat $chat): ChatResource
    {
        ChatResource::wrap('chat');
        return new ChatResource($chat);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreChat  $request
     * @return JsonResponse
     */
    public function store(StoreChat $request): JsonResponse
    {
        $host = request()->getHttpHost();
        $role = null;

        if ($host == config('services.base_hosts.patient')) {
            $role = 'patient';
        } elseif ($host == config('services.base_hosts.hcp')) {
            $role = 'hcp';
        }

        CreateChat::dispatch(
            Auth::user()->getId(),
            $role,
            $request->input('chat.users')
        );

        return response()->json([
            'status' => 'Chat accepted'
        ]);
    }
}
