<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreMessage;
use App\Http\Resources\Message\MessageCollection;
use App\Jobs\CreateMessage;
use App\Models\Chat;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class MessageController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  StoreMessage  $request
     * @param  Chat          $chat
     *
     * @return JsonResponse
     */
    public function store(StoreMessage $request, Chat $chat): JsonResponse
    {
        $host = request()->getHttpHost();
        $role = null;

        if ($host == config('services.base_hosts.patient')) {
            $role = 'patient';
        } elseif ($host == config('services.base_hosts.hcp')) {
            $role = 'hcp';
        }

        CreateMessage::dispatch(
            $chat->id,
            Auth::user()->getId(),
            $role,
            request()->bearerToken(),
            $request->input('message.content')
        );

        return response()->json([
            'status' => 'Message accepted'
        ]);
    }

    /**
     * Fetch the messages for the given chat.
     *
     * @param  Chat  $chat
     * @return MessageCollection
     */
    public function fetchMessages(Chat $chat): MessageCollection
    {
        $messages = $chat->messages
            ->sortBy('created_at');

        MessageCollection::wrap('messages');
        return new MessageCollection($messages);
    }
}
