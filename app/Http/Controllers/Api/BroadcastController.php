<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\InitBroadcast;
use Pusher\Pusher;
use Pusher\PusherException;

class BroadcastController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  InitBroadcast  $request
     * @return string
     */
    public function broadcast(InitBroadcast $request): string
    {
        try {
            $pusher = new Pusher(
                config('broadcasting.connections.pusher.key'),
                config('broadcasting.connections.pusher.secret'),
                config('broadcasting.connections.pusher.app_id')
            );

            $result = $pusher->socket_auth(
                $request->get('channel_name'),
                $request->get('socket_id')
            );
        } catch (PusherException $exception) {
            $result = response()->json([
                'status' => 'Error initializing Pusher.'
            ], 500);
        }

        return $result;
    }
}
