<?php

namespace App\Jobs;

use App\Events\ChatCreated;
use App\Models\Chat;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CreateChat implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The scope of the user who is creating the chat.
     *
     * @var string
     */
    private $initiatorUserScope;

    /**
     * The ID of the user who is creating the chat.
     *
     * @var string
     */
    private $initiatorUserID;

    /**
     * The users to add to the chat.
     *
     * @var array
     */
    private $users;

    /**
     * Create a new job instance.
     *
     * @param  string  $initiatorUserID
     * @param  string  $initiatorUserScope
     * @param  array   $users
     */
    public function __construct(string $initiatorUserID, string $initiatorUserScope, array $users)
    {
        $this->initiatorUserID = $initiatorUserID;
        $this->initiatorUserScope = $initiatorUserScope;
        $this->users = $users;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->users[] = [
            'user_id' => $this->initiatorUserID,
            'scope' => $this->initiatorUserScope
        ];

        $chat = Chat::create([
            'users' => $this->users,
        ]);

        event(new ChatCreated($chat));
    }
}
