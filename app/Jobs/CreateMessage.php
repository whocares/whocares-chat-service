<?php

namespace App\Jobs;

use App\Events\MessageSent;
use App\Http\Requests\StoreChat;
use App\Models\Message;
use Exception;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;

class CreateMessage implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Request object.
     *
     * @var StoreChat
     */
    private $request;

    /**
     * The ID of the chat where the message is sent to.
     *
     * @var string
     */
    private $chatID;

    /**
     * The message to store.
     *
     * @var string
     */
    private $message;

    /**
     * The ID of the user who sent the message.
     *
     * @var string
     */
    private $userID;

    /**
     * The scope of the user who sent the message.
     *
     * @var string
     */
    private $scope;

    /**
     * The token of the user who sent the message.
     *
     * @var string
     */
    private $token;

    /**
     * Create a new job instance.
     *
     * @param  string  $chatID
     * @param  string  $userID
     * @param  string  $scope
     * @param  string  $token
     * @param  string  $message
     */
    public function __construct(string $chatID, string $userID, string $scope, string $token, string $message)
    {
        $this->chatID = $chatID;
        $this->userID = $userID;
        $this->scope = $scope;
        $this->token = $token;
        $this->message = $message;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $name = null;

        if ($this->scope == 'patient') {
            $name = $this->getPatientName();
        } elseif ($this->scope == 'hcp') {
            $name = $this->getHCPName();
        }

        $message = Message::create([
            'chat_id' => $this->chatID,
            'user_id' => $this->userID,
            'content' => $this->message,
        ]);

        event(new MessageSent($message, $name));
    }

    /**
     * Enrich user data with the patients data.
     *
     * @return string
     */
    private function getPatientName(): ?string
    {
        $url = config('internal_api.patient_service_url') . "/api/v1/patient-service/patients/user/{$this->userID}";

        try {
            $result = Http::withoutVerifying()
                ->withToken($this->token)
                ->get($url);

            logger($result);

            return $result['patient']['first_name'] . ' ' . $result['patient']['last_name'];
        } catch (Exception $exception) {
            logger($exception);
        }

        return null;
    }

    /**
     * Enrich user data with the healthcare providers data.
     *
     * @return string
     */
    private function getHCPName(): ?string
    {
        $url = config('internal_api.hcp_service_url') . "/api/v1/hcp-service/healthcare-providers/user/{$this->userID}";

        try {
            $result = Http::withoutVerifying()
                ->withToken($this->token)
                ->get($url);

            logger($result);

            return $result['healthcareProvider']['first_name'] . ' ' . $result['healthcareProvider']['last_name'];
        } catch (Exception $exception) {
            logger($exception);
        }

        return null;
    }
}
