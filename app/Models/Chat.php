<?php

namespace App\Models;

use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Jenssegers\Mongodb\Eloquent\Model;
use Jenssegers\Mongodb\Relations\HasMany;

class Chat extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mongodb';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'users',
        'patient_name',
        'patient',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at',
        'updated_at',
    ];

    /**
     * Get all of the messages related to the chat.
     *
     * @return HasMany
     */
    public function messages(): HasMany
    {
        return $this->hasMany(Message::class);
    }

    /**
     * Find the name for the related users.
     *
     * @param  array  $users
     * @return array
     */
    public function getUsersAttribute(array $users): array
    {
        foreach ($users as $key => &$user) {
            $authUser = Auth::user();

            logger($user);

            if (isset($authUser) && $user['user_id'] === $authUser->getId()) {
                unset($users[$key]);
                continue;
            }

            if ($user['scope'] == 'patient') {
                $user = $this->enrichPatient($user);
            }

            if ($user['scope'] == 'hcp') {
                logger('enricht hcp');
                $user = $this->enrichHealthcareProvider($user);
            }
        }

        return $users;
    }

    /**
     * Enrich user data with the patients data.
     *
     * @param  array  $user
     * @return array
     */
    private function enrichPatient(array $user): array
    {
        $host = request()->getHttpHost();
        $base = null;

        if ($host == config('services.base_hosts.patient')) {
            $base = config('internal_api.patient_service_url');
        } elseif ($host == config('services.base_hosts.hcp')) {
            $base = config('internal_api.hcp_service_url');
        }

        if (isset($base)) {
            $url = $base . "/api/v1/patient-service/patients/user/{$user['user_id']}";

            try {
                $result = Http::withoutVerifying()
                    ->withToken(request()->bearerToken())
                    ->get($url);

                logger($result);

                $user['first_name'] = $result['patient']['first_name'];
                $user['last_name_prefix'] = $result['patient']['last_name_prefix'];
                $user['last_name'] = $result['patient']['last_name'];

            } catch (Exception $exception) {

                $user['first_name'] = null;
                $user['last_name_prefix'] = null;
                $user['last_name'] = null;

                logger($exception);

            }
        }

        return $user;
    }

    /**
     * Enrich user data with the healthcare providers data.
     *
     * @param  array  $user
     * @return array
     */
    private function enrichHealthcareProvider(array $user): array
    {
        $host = request()->getHttpHost();
        $base = null;

        logger($host);

        if ($host == config('services.base_hosts.patient')) {
            $base = config('internal_api.patient_service_url');
        } elseif ($host == config('services.base_hosts.hcp')) {
            $base = config('internal_api.hcp_service_url');
        }

        logger($base);

        if (isset($base)) {
            $url = $base . "/api/v1/hcp-service/healthcare-providers/user/{$user['user_id']}";

            try {
                $result = Http::withoutVerifying()
                    ->withToken(request()->bearerToken())
                    ->get($url);

                logger($result);

                $user['first_name'] = $result['healthcareProvider']['first_name'];
                $user['last_name'] = $result['healthcareProvider']['last_name'];

            } catch (Exception $exception) {

                $user['first_name'] = null;
                $user['last_name'] = null;

                logger($exception);

            }
        }

        return $user;
    }
}
